# AppsRepositorio

Repositorio creado con la finalidad de revisión de prueba de trabajo.

## MyApp

Aplicación desarrollada usando Ionic, la cual consiste en hacer la autenticación de usuario haciendo uso de la API (https://dev.tuten.cl:443/TutenREST/#!/user/login) y  haciendo uso de datos alojados en la API (https://dev.tuten.cl:443/TutenREST/#!/user/bookings). Se utilizo Angular versión 5.2.10 e Ionic versió.n 3.9.2. Para ejecutar está aplicación debe ejecutar este comando en la consola de git ** git clone git@bitbucket.org:ArturoEscalona/appsrepositorio.git ** seguidamente en la consola del sistema ejecutara el siguiente comando ** npm install -g ionic ** y posteriormente al complementarse con éxito dicho comando, puede proceder a ejecutar la aplicación con el comando  ** ionic serve **. La aplicación fue desarrollada en SO Windows pero puede ser ejecutada en Linux (Vea documentación para los comandos de instalación del framework en la siguiente página web ** https://ionicframework.com/docs/cli/ ** ).