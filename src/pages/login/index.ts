import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { IndexServiceProvider } from '../../providers/index-service/index-service';
import { HomePage } from '../home/home';

@Component({
  selector: 'page-index',
  templateUrl: 'index.html'
})
export class LoginIndex {
  responseData : any;
  userData = {"email" : "", "password" : "", "app" : "APP_WEB"};
  userAPI = {"email" : "testapis@tuten.cl", "password" : "1234", "app" : "APP_BCK"};
  LoginForm: FormGroup;

constructor(public navCtrl: NavController, public IndexServiceProvider:IndexServiceProvider, public fb: FormBuilder) {
    this.getToken();
    if (typeof(localStorage.getItem("userData")) == 'string') {
      this.navCtrl.push(HomePage);
    }
    this.LoginForm = this.createForm();
  }

  login(){
    this.IndexServiceProvider.LoginFromApi(this.userData.email, this.userData.password, this.userData.app).then((result) => {
      this.responseData = result;
      localStorage.setItem('userData', JSON.stringify(this.responseData));
      this.navCtrl.push(HomePage);
    }, (err) => {
      alert('Ocurrio un problema al enviar el formulario');
    });
  }

  getToken() {
    
    this.IndexServiceProvider.LoginFromApi(this.userAPI.email, this.userAPI.password, this.userAPI.app).then((result) => {
      this.responseData = result;
      localStorage.setItem('userAPI', JSON.stringify(this.responseData));
    }, (err) => {

    });
  }

  private createForm(){
    return this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]],
    });
  }

}

