import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { HttpModule } from '@angular/http';
import { MyApp } from './app.component';

import { LoginIndex } from '../pages/login/index';
import { HomePage } from '../pages/home/home';
import { SearchFilter } from '../filters/search/search';
import { IndexServiceProvider } from '../providers/index-service/index-service';
import { BookingServiceProvider } from '../providers/booking-service/booking-service';

@NgModule({
  declarations: [
    MyApp,
    LoginIndex,
    HomePage,
    SearchFilter
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginIndex,
    HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    IndexServiceProvider,
    BookingServiceProvider
  ]
})
export class AppModule {}
