import { NgModule } from '@angular/core';
import { SearchFilter } from './search/search';
@NgModule({
	declarations: [SearchFilter],
	imports: [],
	exports: [SearchFilter]
})
export class FiltersModule {}